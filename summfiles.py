"""Yield a stream of summary file paths, either in the last
N days or N weeks. Filter by name, if necessary."""

import os
import glob
import time
import summfile
from collections import defaultdict


SFILE_DIR = '/afs/cern.ch/atlas/project/RTT/prod/summaryfiles'
ONE_DAY = 24 * 3600
ONE_WEEK = 24 * 7 * 3600


def getfilesInRange(fname='*', start=1, stop=0, asChunks=False):
    binsize = None if not asChunks else 1  # in days
    return _getfiles(fname, start, stop, binsize)


def getfilesDaysAgo(daysAgo=1, fname='*', asChunks=False):
    binsize = None if not asChunks else 1  # in days
    return _getfiles(fname, start=daysAgo, stop=0, binsizeInDays=binsize)


def getfilesWeeksAgo(weeksAgo=1, fname='*', asChunks=False):
    """Return a stream of summary files from the last N weeks.
    If desired, rather than yielding one summary file at a time,
    yield a list of them by week chunks.

    :param weeksAgo: how many weeks back to fetch summary files
    :param fname: a unix glob to filter summary file names on
    :param asChunks: if True, yield lists of summary files for each week
    """
    daysAgo = weeksAgo * 7
    binsize = None if not asChunks else 7  # in days
    return _getfiles(fname, start=daysAgo, stop=0, binsizeInDays=binsize)


def getJobs(startdays=1, stopdays=0):
    """Yield a stream of minder elements from
    runs between @start and stopdays."""
    for sf in getfilesInRange(start=startdays, stop=stopdays):
        for job in summfile.jobs(sf):
            yield job


def _age(sfname, timebin):
    return int((time.time() - int(sfname.split('_')[0])) / (timebin * 1.0))


def ageInDays(sfname):
    return _age(sfname, ONE_DAY)


def ageInWeeks(sfname):
    return _age(sfname, ONE_WEEK)


def allfiles(fname='*'):
    fname = '1' + fname + '.xml'
    for f in glob.glob(os.path.join(SFILE_DIR, fname)):
        yield f


def _filesInRange(sfiles, start, stop):
    """Filter a list of summary files on age,
    returning a stream of only those that are
    within a range of days old. The range is inclusive
    on both ends i.e. start >= age >= stop passes
    the age filter.

    :param sfiles: the list of summary file paths
    :param start: integer days
    :param stop: integer days
    """
    for sfpath in sfiles:
        sfname = os.path.basename(sfpath)
        age = ageInDays(sfname)
        if start >= age >= stop:
            yield (age, sfpath)


def _getfiles(fname, start, stop, binsizeInDays=None):
    bins = defaultdict(list)
    summfiles = allfiles(fname)
    for ageInDays, sfpath in _filesInRange(summfiles, start, stop):
        if not binsizeInDays:
            yield sfpath
        else:
            binno = ageInDays / binsizeInDays
            bins[binno].append(sfpath)

    for bin in sorted(bins):
        yield bins[bin]
