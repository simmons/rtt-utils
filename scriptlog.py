"""Parse a job script log for various info
such as execution host, queue, job duration etc.
and return it as a namedtuple called ScriptLog.
"""

import re
import calendar
import time
import datetime
from utilities import filelines, bag


node = re.compile('Job was executed on host\(s\) <(.*?)>')
queue = re.compile('in queue <(.*?)>')
started = re.compile('Started at (.*)')
stopped = re.compile('Results reported at (.*)')
cputime = re.compile('CPU time   :\s+(.*?) sec.')
maxmem = re.compile('Max Memory :\s+(\d+) MB')
pid = re.compile('Subject: Job\s+(\d+):')


def parse(logpath):
    info = {}
    regexes = [('node', node, None),
                ('queue', queue, None),
                ('start', started, _toEpoch),
                ('stop', stopped, _toEpoch),
                ('pid', pid, int),
                ('maxmem', maxmem, int),
                ('cputime', cputime, float)]

    for line in filelines(logpath):
        _fillInfo(info, line, regexes)
        if len(regexes) == len(info):
            break
    else:
        info['cputime'] = 0
        info['maxmem'] = 0

    info['duration'] = info['stop'] - info['start']
    info['path'] = logpath
    return bag('ScriptLog', **info)


def jobduration(logpath):
    info = parse(logpath)
    return (info['stop'] - info['start'])


def _fillInfo(info, line, regexes):
    for rname, regex, func in regexes:
        m = regex.search(line)
        if m:
            groups = m.groups()
            value = groups[0] if groups else m.group()
            value = value if func is None else func(value)
            info[rname] = value


def _toEpoch(s):
    # e.g. convert Wed Dec 11 06:16:46 2013 to an epoch time
    dayname, monthname, day, time_, year = s.split()
    day = int(day)
    year = int(year)
    month = [i for i, n in enumerate(calendar.month_abbr) if n == monthname][0]
    hours, mins, secs = [int(i) for i in time_.split(':')]
    dt = datetime.datetime(year, month, day, hours, mins, secs)
    return int(time.mktime(dt.timetuple()))


if __name__ == '__main__':
    import sys
    log = sys.argv[1]
    print(parse(log))
