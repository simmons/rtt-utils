"""Find all RTT batch hosts using the lshosts command."""

from collections import namedtuple
from shell import shell


def lshosts(*nodes):
    for node in nodes:
        node = node.replace('.cern.ch', '')
        info = shell('lshosts | grep %s' % node)[0].strip()
        yield parseInfo(info)


def parseInfo(*lines):
    """Take a list of stdout lines from lshosts command,
    and return for each one a namedtuple created after parsing the line.
    If only one line is provided, return a named tuple, else
    return a list of namedtuples.

    :param lines: an arg list of lshosts stdout lines
    """
    Node = namedtuple('Node', 'name os model ncpus mem swap iswigner')
    info = []
    for line in lines:
        toks = line.split()
        nodename = toks[0]
        ostype = 'slc5' if 'slc5' in toks[1] else 'slc6'
        model = toks[2]
        ncpus = toks[4]
        mem = toks[5]
        swap = toks[6]
        resources = ' '.join(toks[8:])
        isWigner = 'wigner' in resources
        info.append(Node(nodename, ostype, model, ncpus, mem, swap, isWigner))

    return info[0] if len(info) == 1 else info
