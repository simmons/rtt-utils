"""Parse an XML file using cElementTree or minidom."""


def treeparse(xf):
    import xml.etree.cElementTree as XML
    return _parse(xf, XML)


def domparse(xf):
    import xml.dom.minidom as XML
    return _parse(xf, XML)


def _parse(xf, module):
    try:
        return module.parse(xf)
    except:
        return None
