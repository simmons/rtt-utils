"""Useful functions that act on summary files
or some child elements of a summary file.
"""

import os
import re
import statehistory
import xmlparse as XP
from utilities import bag


def parse(sfpath):
    """Parse the summary file and return either an
    XML tree object, or None if the file is unparseable.

    :param sfpath: path to the summary file
    """
    t = XP.treeparse(sfpath)
    if t is None:
        print('Unable to parse: {0}'.format(sfpath))
    return t


def parseName(sfpath):
    """Parse a summfile name into its constituents,
    returning a namedtuple.

    :param sfpath:  path to the summary file
    """
    fname = os.path.basename(sfpath)
    patt = '(\d{10})_(\w{3})_(rel_\d)_(.*?)_build_(.*)_(.*?).xml'
    cols = 'start framework release branch cmtconfig project'
    cp = re.compile(patt)
    match = cp.match(fname)
    if not match:
        return None
    zipped = zip(cols.split(), match.groups())
    d = dict(zipped)
    return bag('SummFileNameInfo', **d)


def jobsInPackage(package):
    return package.getiterator('minder')


def errJobsInPackage(package):
    return package.getiterator('errorMinder')


def jobs(sfpath):
    return getTag('minder', sfpath, gettagtext=False)


def errJobs(sfpath):
    return getTag('errorMinder', sfpath, gettagtext=False)


def getTag(tagname, sfpath, allmatches=True, gettagtext=True):
    """Parse a summary file and return one or all matches for
    a given tag. The return values are either the tag(s) or the
    text content of the tag(s).

    :param tagname: the XML tag to search for
    :param sfpath: path to the summary file
    :param allmatches: return all matches if True else the first only
    :param gettagtext: return text content of match tags or the tags
    """
    t = parse(sfpath)
    if t:
        func = t.findall if allmatches else t.find
        for tag in func('.//' + tagname):
            yield tag.text if gettagtext else tag


def batchPIDs(sfpath):
    """Return a stream of the batch PIDs of all jobs in a summary file.

    :param sfpath: path to the summary file
    """
    for pid in getTag('batchPID', sfpath):
        yield pid


def resourceUsage(el):
    """Pass in an element - can be a job or a package - and
    return all resourcesUsage nodes as a list of dicts. Return
    an empty list if none are found.

    :param el: the XML element below which to find resource els
    """
    nodes = []
    for usageEl in el.findall('.//resourceUsage'):
        node = {}
        for child in ('exechost',
                      'cpuSeconds',
                      'ksi2kSeconds',
                      'memInMB',
                      'swapInMB'):
            val = usageEl.findtext(child)
            if not val:
                break
            if child == 'exechost':
                val = val.replace('.cern.ch', '')

            node[child] = int(val) if child != 'exechost' else str(val)
        nodes.append(node)
    return nodes


def walltime(job):
    """For a given job, return the time spent pending in batch.

    :param job: the job XML element
    """
    sh = job.findtext('stateHistory')
    history = statehistory.parse(sh)
    try:
        rib = history.runningInBatch
        term = history.terminate
    except:
        return -1
    return term - rib


def resultsPaths(sfpath, onlyExisting=True):
    """Return a stream of results directory paths taken
    from a summary file. By default, return only existing paths.

    :param sfpath: the path to the summary file
    :param onlyExisting: if False, return even non existant paths
    """
    for rp in getTag('resultsPath', sfpath):
        if not onlyExisting or os.path.exists(rp):
            yield rp


def joblogs(sfpath, onlyExisting=True):
    """Return a stream of paths to job logs from
    a summary file. By default only return existing logs.

    :param sfpath: the path to the summary file
    :param onlyExisting: if False, return even non existant paths
    """
    for respath in resultsPaths(sfpath):
        jobname = os.path.basename(respath)
        joblog = '{0}_log'.format(os.path.join(respath, jobname))
        if not onlyExisting or os.path.exists(joblog):
            yield joblog


def scriptlogs(sfpath, onlyExisting=True):
    """Return a stream of paths to job script logs from
    a summary file. By default only return existing logs.

    :param sfpath: the path to the summary file
    :param onlyExisting: if False, return even non existant paths
    """
    for respath in resultsPaths(sfpath):
        jobname = os.path.basename(respath)
        log = '{0}_script_log'.format(os.path.join(respath, jobname))
        if not onlyExisting or os.path.exists(log):
            yield log


def stateHistories(sfpath):
    for sh in getTag('minder/stateHistory', sfpath):
        yield sh


class SummFile(object):
    def __init__(self, path):
        self.path = path
        self.tree = parse(path)

    def overview(self):
        pass

    def failpackages(self):
        els = self.tree.findall('.//package')
        return [FailPackage(p) for p in els]

    def packages(self, *names):
        """Return a list of package XML elements. If there
        are package names provided, only return package elements
        whose <packageName> is in the list.

        :param names - an arg list of package names to match
        """
        els = self.tree.findall('.//package')
        if names:
            matches = []
            for el in els:
                if el.findtext('packageName') in names:
                    matches.append(el)
            els = matches
        return [Package(p) for p in els]


class PackageBase(object):
    def __init__(self, el):
        self.el = el
        self.name = el.findtext('packageName')
        self.tag = el.findtext('packageTag')
        self.container = el.findtext('containerPackage')
        self.xmlfile = el.findtext('pathToTestConfig')


class FailPackage(PackageBase):
    def __init__(self, el):
        PackageBase.__init__(self, el)
        self.vetoed = el.find('vetoed') is not None
        if not self.vetoed:
            # load exceptions
            self._exceptions()

    def _exceptions(self):
        warnings = self.el.findall('.//warningExceptions/exception')
        errors = self.el.findall('.//exceptions/exception')
        self.warnings = [getChildNodesDict(e) for e in warnings]
        self.errors = [getChildNodesDict(e) for e in errors]


class Package(PackageBase):
    def __init__(self, el):
        PackageBase.__init__(self, el)
        self.jobs = [Job(j) for j in el.findall('.//minder')]
        self.errjobs = [ErrorJob(j) for j in el.findall('.//errorMinder')]


class JobBase(object):
    def __init__(self, el):
        self.el = el
        self.name = el.findtext('jobName')
        self.group = el.findtext('jobGroup')


class ErrorJob(JobBase):
    def __init__(self, el):
        JobBase.__init__(self, el)


class Job(JobBase):
    def __init__(self, el):
        JobBase.__init__(self, el)
        self.pid = el.findtext('batchPID')
        self.queue = el.findtext('jobQueue')
        self.exitcode = el.findtext('jobExitCode')
        self.state = el.findtext('state')
        self.history = el.findtext('stateHistory')
        self.respath = el.findtext('resultsPath')
        self.__dict__.update(self._usage())

    def _usage(self):
        ru = self.el.find('resourceUsage')
        return getChildNodesDict(ru)

    def walltime(self):
        history = statehistory.parse(self.history)
        try:
            rib = history.runningInBatch
            term = history.terminate
        except:
            return -1
        return term - rib


def getChildNodesDict(parentEl):
    d = {}
    for node in parentEl.getchildren():
        d[node.tag] = node.text
    return d
