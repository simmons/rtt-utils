"""Create acron objects from each acron entry in the table."""

import re
import os
import shell as S
import nicos
import datetime


def acrontable(patt='.*'):
    """Return a stream of regex match instances from the acrontable that
    match a given pattern. The default regex matches all lines.

    :param patt: the regex pattern to compile and match against
    """
    regex = re.compile(patt)
    for line in S.shell('acrontab -l'):
        line = line.strip()
        if line.startswith('#'):
            continue
        sre = regex.match(line)
        if sre:
            yield sre


class AcronProdRun(object):
    def __init__(self, mins, hours, host, run):
        self.mins = int(mins)
        self.hour = int(hours)
        self.host = host
        self.run = run
        toks = run.split('/')
        self.framework, self.branch, self.cmtconfig, self.project = toks
        self.runWithNoFramework = os.path.join(self.branch,
                                               self.cmtconfig,
                                               self.project)

    def shouldBeActive(self):
        """Is the acron time in between nicos midnight and now?
        If True, then should be running, else False.
        """
        now = datetime.datetime.now()
        shouldBe = True
        if now.hour == self.hour:
            shouldBe = (self.mins > now.minute)
        else:
            nicosNewDay = nicos.midnight(now)
            diffNow = now.hour - nicosNewDay.hour
            diffNow = diffNow if diffNow >= 0 else (diffNow + 24)
            diffAcron = self.hour - nicosNewDay.hour
            diffAcron = diffAcron if diffAcron >= 0 else (diffAcron + 24)
            shouldBe = diffAcron < diffNow
        return shouldBe

    def __str__(self):
        return self.run


def currentProdRuns(ignoreFramework=False):
    """Parse the acron table into AcronProdRun instances
    for each entry and return them in a list.

    :param ignoreFramework: if True, include only one instance of runs whose
                            difference is solely the framework.
    """
    patt = ('(\d\d)'  # mins
            '\s+'
            '(\d\d)'  # hours
            '.*?'
            '(airtt\d\d\d)'  # host
            '.*?RTTStart.py prod.*?\s+'
            '(.*?\/.*?\/.*?\/.*?)\s+')  # run frag

    runs = []
    for match in acrontable(patt):
        run = AcronProdRun(*match.groups())
        if not ignoreFramework:
            runs.append(run)
        else:
            if not [r for r in runs if
                    r.runWithNoFramework == run.runWithNoFramework]:
                runs.append(run)
    return runs
