"""Yield a stream of script log files either from
a list of provided summary files, or dating from the last N days.
"""

import os
import summfile
import summfiles


def _get(sfiles):
    for sfile in sfiles:
        for rp in summfile.resultsPaths(sfile):
            jobname = os.path.basename(rp)
            log = '%s_script_log' % jobname
            log = os.path.join(rp, log)
            if os.path.exists(log):
                yield log


def sinceDaysAgo(ndays=1):
    """Find all script logs in the last ndays, returning
    a stream of the matches.

    :param ndays: integer days to go back from today (default: 1)
    """
    sfiles = summfiles.getfilesDaysAgo(daysAgo=ndays)
    return _get(sfiles)


def fromSummFiles(sfiles):
    return _get(sfiles)
