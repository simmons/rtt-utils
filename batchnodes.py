"""Get the RTT batch nodes according to lshosts command."""

from __future__ import print_function
from shell import shell
from collections import defaultdict
from utilities import bag
from functools import partial


def get():
    nodes = shell("lshosts | grep atlasrtt")
    host = partial(bag, 'BatchNode')
    for n in nodes:
        toks = n.strip().split()
        resources = ' '.join(toks[7:])
        h = host(name=toks[0],
                 system=toks[1],
                 model=toks[2],
                 cpufactor=float(toks[3]) if toks[3] else 0,
                 ncpus=int(toks[4]) if toks[4] != '-' else 0,
                 ram=toBytes(toks[5]) if toks[5] != '-' else 0,
                 swap=toBytes(toks[6]) if toks[6] != '-' else 0,
                 at_wigner='wigner' in resources,
                 is_intel='intel' in resources)
        yield h


def toBytes(s):
    # assume s of the form 1234M
    return int(s[:-1])*1024*1024

def toGB(s):
    return int(round(int(s)/(1024.0**3)))

def totalRAM():
    return sum(host.ram for host in get())

def stats():
    ram = ncpus = swap = 0
    intel = 0
    bad = []
    wigner = []
    non_intel = []
    nhosts = 0

    ncpusCount = defaultdict(int)
    ramCount = defaultdict(int)

    for host in get():
        nhosts += 1

        if host.is_intel:
            intel += 1
        else:
            non_intel.append(host.name)

        if not host.ram or not host.ncpus or not host.swap:
            bad.append(host.name)
            continue

        ramCount[host.ram] += 1
        ncpusCount[host.ncpus] += 1

        ram += host.ram
        ncpus += host.ncpus

        if host.at_wigner:
            wigner.append(host.name)

    cpuBreakDown = ', '.join(['{0}x{1}'.format(ncpusCount[n], n) for n in sorted(ncpusCount)])
    ramBreakDown = ', '.join(['{0}x{1}GB'.format(ramCount[n], toGB(n)) for n in sorted(ramCount)])
        

    print('----- RTT BATCH NODES ----------------------')
    print('Total number nodes: {0}'.format(nhosts))
    print('  ...at CERN: {0}'.format((nhosts-len(wigner))))
    print('  ...at Wigner: {0}'.format(len(wigner)))
    print('  ...are Intel: {0}'.format(intel))
    print('Total CPUs: {0} ({1})'.format(ncpus, cpuBreakDown))
    print('Total RAM : {0}GB ({1})'.format(toGB(ram), ramBreakDown))
    print('--------------------------------------------')
    if bad:
        print('Bad nodes: {0}'.format(', '.join(bad)))
    
    if non_intel:
        print('Non-Intel nodes: {0}'.format(', '.join(non_intel)))


if __name__ == '__main__':
    stats()
