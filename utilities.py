"""Helper functions"""

import os
import time
import datetime
import calendar
import itertools
import collections


def filelines(fpath):
    if not os.path.exists(fpath):
        return

    with open(fpath) as f:
        for line in f:
            yield line.strip()


def grepfile(fpath, text, allmatches=True):
    for line in filelines(fpath):
        if text in line:
            yield line
            if not allmatches:
                break


def projMap(project):
    project = {'offline': 'AtlasProduction',
               'AtlasProduction': 'offline',
               'p1hlt': 'AtlasP1HLT',
               'AtlasP1HLT': 'p1hlt',
               'hlt': 'AtlasHLT',
               'AtlasHLT': 'hlt'}.get(project, project)
    return project


def orderedbag(klassname, keyvalpairs):
    fields = []
    values = []
    for key, val in keyvalpairs:
        fields.append(key)
        values.append(val)
    fields = ' '.join(fields)
    Klass = collections.namedtuple(klassname, fields)
    return Klass(*values)


def bag(klassname, **namevalues):
    fields = ' '.join(namevalues.keys())
    values = namevalues.values()
    Klass = collections.namedtuple(klassname, fields)
    return Klass(*values)


def take(iterator, n):
    """Take the first @n values of @iterator."""
    values = list(itertools.islice(iterator, n))
    return values if n > 1 else values[0]


def datetimeToEpoch(dt):
    return int(time.mktime(dt.timetuple()))


def epochToDatetime(epoch):
    return datetime.datetime.fromtimestamp(epoch)


def monthname(monthIndex):
    names = []
    for i, mn in enumerate(calendar.month_abbr):
        if i == monthIndex:
            names.append(mn)
    return names[0]


def histogramify(data, binsize):
    bins = collections.defaultdict(int)
    for d in data:
        binNo = int(d / (binsize * 1.0))
        bins[binNo] += 1
    return bins
