"""Parse the content of the state history XML element."""

import utilities
import datetime
import re


def toEpoch(s):
    """Convert string of form 15/10/19 04:32 to an epoch time.

    :param s: input datetime string to be converted
    """
    cp = re.compile('(\d\d)\/(\d\d)\/(\d\d)\s+(\d\d):(\d\d)')
    matches = cp.match(s).groups()
    matches = [int(m) for m in matches]
    year, month, day, hour, mins = matches
    year += 2000
    dt = datetime.datetime(year, month, day, hour, mins)
    return utilities.datetimeToEpoch(dt)


def parse(history):
    """Convert a statehistory XML tag text content of the form
    '[(state, datetime),...]' into a named tuple with fields equal
    to states and values equal to epoch time stamps.

    :param history: the string repr of the statehistory list
    """
    histdict = {}
    for state, datetimeOrEpoch in eval(history):
        epoch = datetimeOrEpoch
        if isinstance(datetimeOrEpoch, str):  # old style
            epoch = toEpoch(datetimeOrEpoch)
        histdict[state] = epoch

    return utilities.bag('History', **histdict)
