"""Get the latest nicos flag for all acron entries."""

import acron
from nicosflag import nicosflag
import time


def getFlags(entries=None):
    if not entries:
        entries = _getCronEntries()

    flagtimes = []
    for entry in entries:
        linktime, _ = nicosflag(entry.branch, entry.cmtconfig, entry.project)
        flagtimes.append((linktime, entry))
    return flagtimes


def _getCronEntries():
    return acron.currentProdRuns(ignoreFramework=True)


def _output(now, flagtime, entry):
    if not flagtime:
        print('UNKNOWN: {0}'.format(entry))
        return

    diff = now - flagtime
    if diff < 24 * 3600:
        print('{0:4} hours ago: {1}'.format(round(diff / 3600., 1), entry))
    else:
        timediff = round(diff / (24 * 3600.), 1)
        print('[OLD] {0} days ago: {1}'.format(timediff, entry))


def main():
    now = time.time()
    flagtimes = getFlags(_getCronEntries())
    for flagtime, entry in sorted(flagtimes):
        # entry e.g. = rtt/devval/x86_64-slc6-gcc49-opt/AtlasProduction
        # remove the framework part
        entry = str(entry)[4:]
        _output(now, flagtime, entry)


if __name__ == '__main__':
    main()
