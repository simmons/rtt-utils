"""Compares acron start time with latest nicos flag availability time,
and prints to stdout those runs where the former comes more than 1 hour
after the latter.
"""

import acron
import constants
from nicosflag import nicosflag
import utilities


NIGHTLIES = constants.AFS_NIGHTLIES_BASE

for entry in acron.currentProdRuns():
    linktime, _ = nicosflag(entry.branch, entry.cmtconfig, entry.project)
    linkDateTime = utilities.epochToDatetime(linktime)
    linkhour = linkDateTime.hour
    diffhour = (abs(linkhour - entry.hour))
    if diffhour > 1 and linkhour < entry.hour:
        m = '{0}, link hour: {1}, acron hour: {2}'.format(entry.run,
                                                          linkhour,
                                                          entry.hour)
        print(m)
