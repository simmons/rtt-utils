"""Parse LSF bjobs command output."""

from utilities import bag
from shell import shell


def parse(pid):
    """Search in the LSF bjobs output for a given PID, and
    return a named tuple. Set the tuple 'exists' boolean according
    to whether the PID is found or not, and an 'error' field to
    the traceback if one occurs when parsing the bjobs output.

    :param pid: the integer batch PID to search for
    """
    answer = shell('bjobs -W {0}'.format(pid), asText=True).strip()
    if 'is not found' in answer:
        return bag('BatchJob', exists=False, error=None)

    try:
        cols, values = answer.split('\n')
    except ValueError as e:
        # wrong number of tuple items
        return bag('BatchJob', error=str(e), exists=False)
    else:
        cols = [c.lower() for c in cols.split()]
        d = dict(zip(cols, values.split()))
        return bag('BatchJob', error=None, exists=True, **d)


def inQueue(pid):
    job = parse(pid)
    return job.exists
