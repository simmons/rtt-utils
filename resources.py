"""Find what results/chainstore dirs are currently
on AFS. See which ones are old for the purpose of cleaning.
"""

from __future__ import print_function
import glob
import os
import re
from collections import defaultdict
import acron
from utilities import projMap


RESBASE = os.environ.get('PROD_RES')
RESREGEX = re.compile(RESBASE + '\/(\w{3})\/rel_\d\/(.*?)\/build\/(.*?)\/(.*)')
CSREGEX = re.compile(RESBASE + '\/(\w{3})\/chainstore\/(.*?)\/(.*?)\/(.*)')
FRAMEWORKS = ('rtt', 'tct', 'fct')


def findResultsPaths(resbase=RESBASE):
    """Glob for existing results paths below a base directory,
    and return a stream of these paths.

    :param resbase: the base dir to use (default $PROD_RES)
    """
    for framework in FRAMEWORKS:
        globpath = os.path.join(RESBASE, framework, 'rel_?/*/build/*/*')
        for respath in glob.glob(globpath):
            yield respath


def findChainstorePaths(resbase=RESBASE):
    """Glob for existing chainstore paths below a base directory
    and return a stream of these paths.

    :param resbase: the base dir to use (default $PROD_RES)
    """
    for framework in FRAMEWORKS:
        globpath = os.path.join(RESBASE, framework, 'chainstore/*/*/*')
        for cspath in glob.glob(globpath):
            yield cspath


def findPathsByRun():
    paths = defaultdict(list)

    for respath in findResultsPaths():
        project = os.path.basename(respath)
        project = projMap(project)
        prefix = os.path.dirname(respath)

        # respath has "offline" at the end,
        # path has "AtlasProduction"
        path = os.path.join(prefix, project)
        run = _pathToRunFrag(path, RESREGEX)
        paths[run].append(respath)

    for cspath in findChainstorePaths():
        run = _pathToRunFrag(cspath, CSREGEX)
        paths[run].append(cspath)

    return paths


def findUnusedPaths():
    """Get all AFS results and chainstore paths, and
    return those that do not correspond to an acron table entry.
    """
    cron = acron.currentProdRuns()
    runs = findPathsByRun()

    unused = {}
    for run in runs:
        for entry in cron:
            if run == entry.run:
                break
        else:
            unused[run] = runs[run]

    # whatever is left in runs dict, is unused
    for run in sorted(unused):
        print(run)
        for path in unused[run]:
            print('\t' + path)

    return unused


def _pathToRunFrag(path, regex):
    run = None
    try:
        groups = regex.match(path).groups()
        framework, branch, cmtconfig, project = groups
        run = '/'.join([framework, branch, cmtconfig, project])
    except:
        pass

    return run
