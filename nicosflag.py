#! /usr/bin/env python

"""Helper functions to find/parse a latest nicos flag."""

import os
import re
import sys
import time


def getArgs():
    branch, project = sys.argv[1:]
    return (branch, project)


def linkName(platform):
    patt = '(.*?)-(slc\d)-(gcc.*?)-(\w\w\w)'
    regex = re.compile(patt)
    bin, slc, gcc, build = regex.match(platform).groups()

    bin = {'x86_64': '64B', 'i686': '32B'}.get(bin)
    slc = {'slc6': 'S6', 'slc5': 'S5'}.get(slc)
    gcc = {'gcc43': 'G4',
           'gcc46': 'G46',
           'gcc47': 'G47',
           'gcc48': 'G48',
           'gcc49': 'G49'}.get(gcc)
    build = {'opt': 'AllOpt', 'dbg': 'AllDbg'}.get(build)

    return 'latest_copied_release%s%s%s%s' % (bin, slc, gcc, build)


def flagpath(branch,
             platform='x86_64-slc6-gcc48-opt',
             project='AtlasProduction'):

    base = '/afs/cern.ch/atlas/software/builds/nightlies/'
    return os.path.join(base, branch, project, linkName(platform))


def nicosflag(branch,
              platform='x86_64-slc6-gcc48-opt',
              project='AtlasProduction'):
    linkpath = flagpath(branch, platform, project)

    try:
        linktgt = os.readlink(linkpath)
    except OSError:  # inexistant
        print('Inexistant: {0}'.format(linkpath))
        linktime = linktgt = None
    else:
        linktime = int(os.lstat(linkpath).st_mtime)

    return linktime, linktgt


def nicosFlagIsOld(branch, platform, project, ndays=1):
    cutoff = time.time() - (ndays * 24 * 3600)
    linktime, linktgt = nicosflag(branch, platform, project)
    return linktime < cutoff


def main():
    branch, project = getArgs()
    print(flagpath(getArgs()))


if __name__ == '__main__':
    main()
