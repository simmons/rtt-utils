"""Parse rtt.job.status files on AFS for state/epoch information."""

import os.path
import re
import calendar
from utilities import filelines, bag
from datetime import datetime as DT


def parse(statusfile):
    """Parse each line of a status file into a dictionary.
    Return a list of named tuples, where each tuple is built
    from a dictionary. Return None if the status file does not
    exist.

    :param statusfile:
    """
    if not os.path.exists(statusfile):
        return None

    instances = {}
    dicts = (_parseline(line) for line in filelines(statusfile))
    dicts = (d for d in dicts if d)
    for d in dicts:
        d['when'] = DT(int(d['year']),
                       _months()[d['month']],
                       int(d['day']),
                       int(d['hour']),
                       int(d['mins']),
                       int(d['secs']))
        del d['year']
        del d['day']
        del d['hour']
        del d['mins']
        del d['secs']
        del d['dayname']
        del d['month']
        instances[d['state']] = d['when']

    return bag('JobStatus', **instances)


def _parseline(line):
    """Parse a line of the job status file, returning a dict."""

    patt = ('(.*?) '
            '\[(\w\w\w) '
            '(.*?)\s+(\d+) '
            '(\d\d):(\d\d):(\d\d) .*? (\d\d\d\d)\]')
    patt = re.compile(patt)
    cols = ('state', 'dayname', 'month',
            'day', 'hour', 'mins', 'secs', 'year')
    d = {}
    try:
        values = patt.match(line).groups()
        d.update(dict(zip(cols, values)))
    except Exception, e:
        print str(e)
        pass

    return d


def _months():
    data = ((month, i) for i, month in enumerate(calendar.month_abbr) if month)
    return dict(data)
