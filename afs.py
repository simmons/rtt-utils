"""AFS helper functions"""

from utilities import bag
from shell import shell
import re


def ismount(path):
    return _getmount(path) is not None


def volsize(path):
    """Return the volume size of an AFS path.
    Return 0 if path is not a mount point.

    :param path: the AFS path
    """
    return getinfo(path)['size']


def isfull(path, triggerlevel):
    """Determine if an AFS path is full or not, as defined by
    a trigger level.

    :param path: the AFS path to examine
    :param triggerlevel: an integer fraction above which the volume is full
    """
    size = getinfo(path)['size']
    return (size is not None and size.frac >= triggerlevel)


def getinfo(path):
    info = {'name': '_', 'size': None, 'ismount': False}
    match = _getmount(path)
    if match:
        volname = match.groups()[0]
        info['name'] = volname.replace('#', '')
        info['size'] = _afsVolumeSize(path)
        info['ismount'] = True

    return info


def delete(afsvol):
    # safety belt
    neverDelete = ['p.atlas.rtt.prod']
    volname = afsvol['name']
    if volname in neverDelete:
        print('Cannot delete protected volume {}'.format(volname))
        return


def _getmount(path):
    cmd = 'fs lsmount -dir %s' % path
    result = shell(cmd)[0]
    patt = ".*? is a mount point for volume '(.*?)'"
    MOUNT_REGEX = re.compile(patt)
    return MOUNT_REGEX.match(result)


def _afsVolumeSize(path):
    cmd = "fs lq %s | sed -n '2p' | awk '{print $2,$3,$4}'" % path
    tot, used, frac = shell(cmd)[0].split()
    tot = int(tot) * 1024
    used = int(used) * 1024
    frac = int(frac.split('%')[0])
    return bag('VolSize', used=used, tot=tot, frac=frac)
