"""Execute a shell command."""

import subprocess as sp


def shell(cmd, asText=False, instantFeedback=False):
    """Execute a command and return result. Stdout and stderr are merged.

    :param cmd: a string representing the command to execute
                (multiple commands ; separated)
    :param asText: return a single text object (default: list of lines)
    :param instantFeedback: print output as it happens, else wait until done.
    """
    proc = sp.Popen(cmd, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)

    get = proc.stdout.readlines
    output = []
    lines = get()
    output.extend(lines)

    if instantFeedback:
        for line in lines:
            print line

    while proc.poll() is None:
        lines = get()
        output.extend(lines)
        if instantFeedback:
            for line in lines:
                print line

    if asText:
        output = ''.join(output)
    return output
