import os
import constants
import datetime as DT
from utilities import datetimeToEpoch, epochToDatetime

ONE_DAY = 24 * 3600


def latestCopiedLink(branch, cmtconfig, project):
    """Given a branch, cmtconfig and project, return the
    full path to the NICOS latest copied release link.

    :param branch: the branch
    :param cmtconfig: the cmtconfig
    :param project: the project
    """
    binary, slc, gcc, build = cmtconfig.split('-')
    binary = '64B' if binary == 'x86_64' else '32B'
    slc = slc.replace('lc', '').upper()
    gcc = gcc.replace('cc', '').upper()
    build = build.capitalize()
    suffix = binary + slc + gcc + 'All' + build
    linkpath = os.path.join(constants.AFS_NIGHTLIES_BASE,
                            branch,
                            project,
                            'latest_copied_release%s' % suffix)
    return linkpath


def latestRelease(nicoslink):
    return os.readlink(nicoslink)


def linkTimeStamp(linkpath):
    """Return a datetime object for the timestamp of
    the nicos latest copied release linkpath.

    :param linkpath: path to the latest copied link
    """
    epoch = os.lstat(linkpath).st_mtime
    return epochToDatetime(epoch)


def buildReadySince(branch, cmtconfig, project):
    """Return a timedelta object representing the time in seconds
    since the nicos build stamp became available for today.
    If not yet available, return None.
    """
    linkpath = latestCopiedLink(branch, cmtconfig, project)
    linkStamp = linkTimeStamp(linkpath)
    nowStamp = DT.datetime.now()
    return nowStamp - linkStamp


def nicosEpochRange(startdays, enddays=0):
    """Return the epoch stamp corresponding to the range
    defined as a start and stop integer number of nicos days ago.

    :param startdays: start nicos days ago
    :param enddays: nicos days ago (defult = 0, which means today)
    """
    epochNicosStartToday = midnight(asEpoch=True)
    start = epochNicosStartToday - (startdays * ONE_DAY)
    end = (epochNicosStartToday + ONE_DAY) - (enddays * ONE_DAY)
    return (start, end)


def nicosDaysToDatetime(nicosdays):
    """Take an integer number of nicos days ago,
    and return the date time that this corresponds to.

    :param nicosdays: integer number of nicos days ago
    """
    start, stop = nicosEpochRange(nicosdays)
    start = start + (7 * 3600)  # add on 7 hours to get past midnight
    return epochToDatetime(start)


def nicosDaysAgo(epoch):
    epochNicosToday = midnight(asEpoch=True)
    if epoch >= epochNicosToday:
        return 0
    else:
        return int((epochNicosToday - epoch) / ONE_DAY) + 1


def midnight(nowstamp=None, asEpoch=False):
    """Return a datetime object corresponding
    to the nicos new day (7pm)."""
    nowstamp = nowstamp if nowstamp is not None else DT.datetime.now()
    nowhour = nowstamp.hour
    hoursSince7pm = (nowhour + 5) if nowhour >= 0 else (nowhour - 19)
    minutesSince7pm = nowstamp.minute
    secondsSince7pm = nowstamp.second
    delta = DT.timedelta(hours=hoursSince7pm,
                         minutes=minutesSince7pm,
                         seconds=secondsSince7pm)
    stamp = nowstamp - delta
    return stamp if not asEpoch else datetimeToEpoch(stamp)


def timestampIsBetween(dt, dt1, dt2):
    return dt1 < dt <= dt2


def buildIsReady(branch, cmtconfig, project):
    """Given a branch, cmtconfig, project, determine
    if the latest copied release stamp is pointing at today's
    release. True = timestamp on the stamp file is in between
    now and 7pm (nicos new day midnight), False otherwise."""
    linkpath = latestCopiedLink(branch, cmtconfig, project)
    linkStamp = linkTimeStamp(linkpath)
    nowStamp = DT.datetime.now()
    nicosNewDayStamp = midnight(nowStamp)
    return timestampIsBetween(linkStamp, nicosNewDayStamp, nowStamp)
