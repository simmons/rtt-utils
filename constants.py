"""Some useful paths."""

import os

AFS_SW_BASE = '/afs/cern.ch/atlas/software/'
AFS_NIGHTLIES_BASE = os.path.join(AFS_SW_BASE, 'builds/nightlies')
